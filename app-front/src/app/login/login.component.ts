import { Component, OnInit } from '@angular/core';
import { Credentials } from '../models/credentials.model';
import { Router  } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { RegisterComponent } from '../register/register.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  credentials: Credentials;
  private baseURL = "http://localhost:8000";
  
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private router: Router, public dialog: MatDialog, private http: HttpClient, private auth: AuthenticationService) {
  }

  ngOnInit(): void {
  }

  onSubmit(event, username: HTMLInputElement, password: HTMLInputElement) {
    event.preventDefault();
  
    this.credentials = new Credentials();
    this.credentials.username = username.value;
    this.credentials.password = password.value;

    this.http.post<Credentials>(this.baseURL +'/api/login', this.credentials, this.httpOptions)
          .subscribe(cred => {
            console.log("login " + cred.username);
            sessionStorage.setItem('username', JSON.stringify(cred.username));

            if(sessionStorage.getItem('username')==="Invalid Credentials") {
              this.auth.loggedStatus.next(null); 
              alert('Invalid Credentials');
            } else {
              this.auth.loggedStatus.next(cred);
            }
            
            this.router.navigate(['home']);
            this.dialog.closeAll();
            
          }, err => alert('Bad credentials'));
    
  }

  register() {
    const dialogRef = this.dialog.open(RegisterComponent, {
      width: '350px',
      height: '300px',
      hasBackdrop: true,
      backdropClass: 'backdropBackground'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }


}



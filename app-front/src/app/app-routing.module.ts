import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component'
import { NewsComponent } from './news/news.component';
import { AboutPaperComponent } from './about-paper/about-paper.component';
import { CoordonatorComponent } from './coordonator/coordonator.component';
import { ProfileComponent } from './profile/profile.component';
import { ContactComponent } from './contact/contact.component';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {path: "login", component: LoginComponent},
  {path: "register", component: RegisterComponent},
  {path: "home", component: HomeComponent},
  {path: "news", component: NewsComponent},
  {path: "about", component: AboutPaperComponent},
  {path: "coordonator", component: CoordonatorComponent},
  {path: "profile", component: ProfileComponent},
  {path: "contact", component: ContactComponent},
  {path: "edit", component: EditProfileComponent},

];
export const appRouting = RouterModule.forRoot(routes);
@NgModule({
  imports: [RouterModule.forRoot(routes), CommonModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }

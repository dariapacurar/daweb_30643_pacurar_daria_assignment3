export class Comment {
    public id: number;
    public username: string;
    public message: string;
    public created_at?: string;
    public updated_at?: string;
}


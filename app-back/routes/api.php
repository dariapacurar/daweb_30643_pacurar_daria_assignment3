<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::get('profile','UserController@index'); // all users
Route::post('update', 'UserController@update'); // update user
Route::post('login', 'UserController@login'); // login
Route::post('register', 'UserController@register'); // register new user
Route::post('logout','UserController@logout'); //logout
Route::post('getUser', 'UserController@getUser');

Route::get('comments', 'CommentsController@index'); // all comments
Route::post('postComment','CommentsController@postComment'); //post new comment

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../services/authentication.service';
import { Credentials } from '../models/credentials.model';
import { Comment } from '../models/comment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-about-paper',
  templateUrl: './about-paper.component.html',
  styleUrls: ['./about-paper.component.scss']
})
export class AboutPaperComponent implements OnInit {
  loggedIn: Credentials;
  comments$: Observable<Comment[]>;
  username: string;
  comment: Comment;
  private baseURL = "http://localhost:8000"; // url din php 
  

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private http: HttpClient, private auth: AuthenticationService, private router: Router) { 
    if (!sessionStorage.getItem('username')) {
      this.router.navigateByUrl('login');
    }
  }

  ngOnInit() {
    this.loggedIn = JSON.parse(sessionStorage.getItem('username')) as Credentials;
    this.comments$ = this.http.get<Comment[]>(this.baseURL + "/api/comments");
  }

  onSubmit(event, message: HTMLInputElement){
    event.preventDefault();
    this.comment = new Comment();
    this.comment.username = String(this.loggedIn);
    this.comment.message = message.value;
    this.http.post<Comment>(this.baseURL + "/api/postComment", JSON.stringify(this.comment), this.httpOptions).toPromise()
    .then(() => alert('success'))
    .catch(() => alert('failure'));
  }


}

<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function index(){
        return User::all();
    }

    public function getUser(Request $request) {
        return User::select('*')->where('username', $request->only('username'))->first();
    }


    // --------------------- [ User login ] ---------------------
    public function login(Request $request) {
        $userCredentials = $request->only('username', 'password');

        if (Auth::attempt($userCredentials)){
            $resp['username'] = $userCredentials['username'];

            return response()->json($resp);
        }

        return response()->json(['error'=>"unauthorized"])->setStatusCode(401);

    }

    // --------------------- [ Create User ] ---------------------
    public function register(Request $request){

        $fields = ['username', 'email', 'password'];
        $credentials = $request->only($fields);
        $validator = Validator::make(
            $credentials,
            [
                'username' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'password' => 'required|min:6',
            ]
        );
        if($validator->fails()){
            return response($validator->errors());
        }

        $result = User::create([
            'username' => $credentials['username'],
            'email' => $credentials['email'],
            'password' => bcrypt($credentials['password']),
        ]);

        if (!empty($result)) {
            return response()->json( $result )->setStatusCode(200);
        }
        return response()->json( "failure" )->setStatusCode(401);
    }

    public function update(Request $request){
        $username = $request['username'];
        $user = User::where('username', $username)->first();
        $user->password = bcrypt($request['password']);
        $user->domains = $request['domains'];
        $user->picture = base64_encode(file_get_contents($request->file('picture')->getRealPath()));
        $user->save();

        return $user;
    }

    // ------------------- [ User logout function ] ----------------------
    public function logout(Request $request ) {
        Session::flush();
        Auth::logout();
        return response()->json("logged out");
    }

}

import { Component, OnInit } from '@angular/core';
import { Credentials } from '../models/credentials.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

  loggedIn: Credentials;
  domains: string[];
  domainsOfInterest: string[];
  finalDomain: string;

  credentials: Credentials;
  personalName: string = "Pacurar Daria";

  private baseURL = "http://localhost:8000"; // url din php 
  fileToUpload: File = null;

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private http: HttpClient, private router: Router) { 
    this.domains = []; 
    this.domainsOfInterest=['OOP', 'Cloud computing', 'Image Processing', 'Mobile apps', 'Internet of Things'];
    if (!sessionStorage.getItem('username')) {
      this.router.navigateByUrl('login');
    } 
  }

  ngOnInit(): void {
    this.loggedIn = JSON.parse(sessionStorage.getItem('username')) as Credentials;
  }

  onSelected(domain: string){
    this.domains.push(domain);
    this.finalDomain = this.domains.join(", ");
  }

  onFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  onSubmit(event, password: HTMLInputElement, files: FileList) {
    event.preventDefault();
    const formData = new FormData();
    console.log(this.finalDomain);
    formData.append('username', String(this.loggedIn));
    formData.append('password', password.value);
    formData.append('domains', this.finalDomain);
    formData.append('picture', this.fileToUpload, 'picture');
  
    this.http.post<Credentials>(this.baseURL + "/api/update", formData).toPromise()
    .then(() => alert('Success'))
    .catch(() => alert('Failure'))

    this.router.navigate(['profile']);
  }



}

import { Component, OnInit } from '@angular/core';
import { Credentials } from '../models/credentials.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  credentials: Credentials;
  private baseURL = "http://localhost:8000"; // url din php 

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
  }

  onRegister(event, email: HTMLInputElement, username: HTMLInputElement,  password: HTMLInputElement) {
    event.preventDefault();
    console.log(email.value, password.value, username.value);

    this.credentials = new Credentials();
    this.credentials.email = email.value;
    this.credentials.password = password.value;
    this.credentials.username = username.value;
    console.log(email, password, username);

    this.http.post<Credentials>(this.baseURL +'/api/register', this.credentials, this.httpOptions)
          .subscribe((register) => 
          {console.log("success user registered");
          alert('Succes')
          this.router.navigate(['login']);
          }, err => alert('eroare'));
    
  }


}

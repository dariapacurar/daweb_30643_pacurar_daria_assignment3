import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MatDialog} from '@angular/material/dialog';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from './services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  copyright: string = "© 2020 Copyright";
  myname: string = "Pacurar Daria";

  isEnglish = false;

  private baseURL = "http://localhost:8000"; // url din php 
  
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private service: TranslateService, public dialog: MatDialog, private http: HttpClient, private auth: AuthenticationService,
    private router: Router) {
  }

  onChangeLanguage() {
    if (this.isEnglish) {
      this.service.use('ro');
    } else {
      this.service.use('en');
    }

    this.isEnglish = !this.isEnglish;
  }

  ngOnInit(): void {
  }

  onLogout() {
    this.http.post(this.baseURL + "/api/logout", this.httpOptions).toPromise()
      .then(() => alert('you\'ve been logged out'))
      .catch(() => alert('Failure'));

    this.auth.loggedStatus.next(null);
    sessionStorage.clear();
    this.router.navigate(['login']);
  }
}

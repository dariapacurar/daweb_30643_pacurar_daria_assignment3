<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class CommentsController extends Controller
{
    public function index(){
        return Comment::all();
    }

    public function postComment(Request $request){
        $fields = ['username', 'message'];
        $comm = $request->only($fields);

        $result = Comment::create([
            'username' => $comm['username'],
            'message' => $comm['message']
        ]);

        if (!empty($result)) {
            return response()->json( $result )->setStatusCode(200);
        }
        return response()->json( "failure" )->setStatusCode(401);
    }
}

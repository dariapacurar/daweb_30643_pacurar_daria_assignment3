# to start the backend php server run:
# php artisan migrate ( to create database )
# php artisan serve ( to start server)

# to start the python mail server (app-backmail) run:
# - FLASK_APP=app.py flask run

# to start client (app-front) run:
# npm install 
# ng serve

# To send email to your own address, in Contact page write your email in the email field.

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-coordonator',
  templateUrl: './coordonator.component.html',
  styleUrls: ['./coordonator.component.scss']
})
export class CoordonatorComponent implements OnInit {
  coordonatorName: string = "Brehar Raluca";
  coordonatorContact: string = "raluca [dot] brehar [at] cs[dot] utcluj[dot] ro";
  constructor(private router: Router) { 
    if (!sessionStorage.getItem('username')) {
      this.router.navigateByUrl('login');
    }
  }

  ngOnInit() {
  }

}

export class Credentials {
    public email: string;
    public username: string;
    public password: string;
    public picture: Blob;
    public domains: string;
}
